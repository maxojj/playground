import os
import time

import requests
from bs4 import BeautifulSoup


class DouBanHouseSpider(object):

    """
    抓取豆瓣小组房源信息
     Attributes:
        force_search: 强力搜索的标志
        key_word: 房源标题关键字
        page_num: 每个小组的抓取页数
        group_list: 豆瓣小组列表
        index_url: 豆瓣小组列表链接
        data: 存放抓取结果
    """
    force_search: int

    def __init__(self,force_search, key_word, page_num):
        self.force_search = force_search
        self.key_word = key_word.split('，')
        self.page_num = page_num
        self.group_list = ['shanghaizufang', 'homeatshanghai', '383972', 'shzf', '251101', '343066', '584379', '612273',
                           '332196']
        self.index_url = [os.path.join('https://www.douban.com/group', i, 'discussion') for i in self.group_list]
        self.data = {}
        print('\n大概了解了, 狗狗祟祟去找房...\n')

    def get_url_content(self, url):
        """
        根据 url 抓取页面数据
        Args:
            url: 豆瓣小组链接
        """
        try:
            time.sleep(1)
            headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_2) AppleWebKit/537.36 '
                                     '(KHTML, like Gecko) Chrome/47.0.2526.80 Safari/537.36'}
            res = requests.get(url, headers=headers)
            soup = BeautifulSoup(res.text, 'html.parser')
            title_list = soup('td', class_='title')
            for tl in title_list:
                if self.force_search == 0:
                    for i in self.key_word:
                        if i in tl.a.attrs['title']:
                            self.data[tl.a.attrs['title']] = tl.a.attrs['href']
                else:
                    if all(word in tl.a.attrs['title'] for word in self.key_word):
                        self.data[tl.a.attrs['title']] = tl.a.attrs['href']
            next_page = soup('span', class_='next')
            if next_page:
                next_url = next_page[0].link.attrs['href']
                end_title = next_url.split('=')[1]
                if int(end_title) < (self.page_num * 25):
                    self.get_url_content(next_url)
        except Exception as e:
            print('好像出错了：%s' % e)

    def start_spider(self):
        """
        爬虫入口
        """
        for i in self.index_url:
            self.get_url_content(i)
        for k, v in self.data.items():
            print('标题：%s, 链接地址：%s'%(k, v))
        print('\n找完收工！暂时就这么多啦，祝你好运~')

def main():
    """
    主函数
    """
    print("""
            ###############################
                豆瓣房源小助手
                作者: 阿森纳是🏆
                版本: 0.0.4
                更新日期: 2019-08-20
            ###############################
        """)
    while True:
        key_word = input('【请输入找房关键词】（多关键词用中文逗号隔开）：')
        page_num = input('【请输入抓取页面数】(数字越大找得越多，也越慢。建议从5开始)：')
        force_search = input('【搜索能力】：1 -关键词必须全部符合；0 -佛系搜索，符合一个即可：')
        if not force_search:
            force_search = 0
        if not key_word:
            key_word = '人民广场'
        if not page_num:
            page_num = 3
        house_spider = DouBanHouseSpider(force_search, key_word, int(page_num))
        house_spider.start_spider()
        greedy = input('不满意的话，调整关键字再来一次？\n 好鸭-1；算了-0：')
        if greedy == '0':
            break
    print('\n希望你有个温暖的家🏠 缘，妙不可言。江湖再会~')


if __name__ == '__main__':
    main()
